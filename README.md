# Gitea Docs ![badge](https://gitea.com/gitea/docs/actions/workflows/build-and-publish.yaml/badge.svg)

## How to build

```shell
make clean
make prepare-docs
make build
```

## Development

```shell
make clean
make prepare-docs
make serve
```

## Test en version

```shell
npm run start
```
