---
date: "2023-03-02T21:00:00+05:00"

slug: "profile-readme"
sidebar_position: 12

---

# Profile READMEs

To display a Markdown file in your Gitea profile page, simply create a repository named `.profile` and add a new file called `README.md`. Gitea will automatically display the contents of the file on your profile, above your repositories.

Making the `.profile` repository private will hide the Profile README.
