---
date: "2016-12-01T16:00:00+02:00"

slug: "install-on-cloud-provider"
sidebar_position: 90

aliases:
  - /en-us/install-on-cloud-provider
---

# Installation on Cloud Provider

## Cloudron

Gitea is available as a 1-click install on [Cloudron](https://cloudron.io).
Cloudron makes it easy to run apps like Gitea on your server and keep them up-to-date and secure.

[![Install](/cloudron.svg)](https://cloudron.io/button.html?app=io.gitea.cloudronapp)

The Gitea package is maintained [here](https://git.cloudron.io/cloudron/gitea-app).

There is a [demo instance](https://my.demo.cloudron.io) (username: cloudron password: cloudron) where
you can experiment with running Gitea.

## Linode

[Linode](https://www.linode.com/) has Gitea as an app in their marketplace.

To deploy Gitea to Linode, have a look at the [Linode Marketplace](https://www.linode.com/marketplace/apps/linode/gitea/).

## alwaysdata

[alwaysdata](https://www.alwaysdata.com/) has Gitea as an app in their marketplace.

To deploy Gitea to alwaysdata, have a look at the [alwaysdata Marketplace](https://www.alwaysdata.com/en/marketplace/gitea/).

## Exoscale

[Exoscale](https://www.exoscale.com/) provides Gitea managed by [Glasskube](https://glasskube.eu/) in their marketplace.

Exoscale is a European cloud service provider.

The package is maintained and update via the open source [Glasskube Kubernetes Operator](https://github.com/glasskube/operator).

To deploy Gitea to Exoscale, have a look at the [Exoscale Marketplace](https://www.exoscale.com/marketplace/listing/glasskube-gitea/).
